import mnist_loader
import red

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
nnet = red.nnet([784,30,10])
nnet.calculateGradient(training_data, 30, 32, 5.0, testData=test_data)