import red
import pickle
import numpy as np

def unpickle(file):
		with open(file, 'rb') as fo:
				dict = pickle.load(fo, encoding='bytes')
		return dict

def merge_dicts(dict1, dict2):
	dict2.pop(b'batch_label')
	keys = dict2.keys()
	return dict((k, np.concatenate((dict1.get(k), dict2.get(k)), axis=0)) for k in keys)

def retrieveBatches(systemPath):
	training_data = merge_dicts(unpickle(systemPath+'data_batch_1'),unpickle(systemPath+'data_batch_2'))
	training_data = merge_dicts(training_data,unpickle(systemPath+'data_batch_3'))
	training_data = merge_dicts(training_data,unpickle(systemPath+'data_batch_4'))
	training_data = merge_dicts(training_data,unpickle(systemPath+'data_batch_1'))
	# training_data = unpickle(systemPath+'data_batch_1')
	test_data = unpickle(systemPath+'test_batch')

	training_data[b'data'] = training_data[b'data'].reshape(50000,3072,1).astype("float64")
	training_data[b'data'] = training_data[b'data'] / 255
	labels = np.zeros((50000,10,1))
	for i, labelIdx in enumerate(training_data[b'labels']):
		labels[i,labelIdx,0] = 1
	training_data[b'labels'] = labels
	training_data = list(zip(training_data[b'data'], training_data[b'labels']))

	test_data[b'data'] = test_data[b'data'].reshape(10000,3072,1).astype("float64")
	test_data[b'data'] = test_data[b'data'] / 255
	test_data = list(zip(test_data[b'data'], test_data[b'labels']))
	return training_data, test_data


systemPath = 'C:/Users/josel/Documents/Informatica/Deep Learning/SVM/cifar-10-python/cifar-10-batches-py/'

training_data, test_data = retrieveBatches(systemPath)
nnet = red.nnet([3072,50,10])
nnet.calculateGradient(training_data, 50, 32, 1.75, testData=test_data)