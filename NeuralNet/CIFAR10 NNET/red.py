import random
import numpy as np

class nnet(object):

    def __init__(self, dims):
        self.numLayers = len(dims)
        self.dims = dims
        self.biases = [np.random.randn(y,1) for y in dims[1:]]
        self.weights = [np.random.randn(y,x) for x, y in zip(dims[:-1], dims[1:])]

    def forward(self, a):
        for b, w in zip(self.biases, self.weights):
            a = sigmoid(np.dot(w,a)+b)
        return a

    def calculateGradient(self, trainingData, iterations, miniBatchSize, ita, testData=None):
        for j in range(iterations):
            random.shuffle(trainingData)
            miniBatches = [trainingData[k:k+miniBatchSize] for k in range(0, len(trainingData), miniBatchSize)]
            for miniBatch in miniBatches:
                self.train(miniBatch, ita)
            if testData:
                print("Iteracion {0}: {1} / {2}".format(j, self.evaluate(testData), len(testData)))
            else:
                print("Iteracion {0} completada".format(j))

    def train(self, miniBatch, ita):
        b_new = [np.zeros(b.shape) for b in self.biases]
        w_new = [np.zeros(w.shape) for w in self.weights]
        for x, y in miniBatch:
            db_new, dw_new = self.backwards(x, y)
            b_new = [nb+dnb for nb, dnb in zip(b_new, db_new)]
            w_new = [nw+dnw for nw, dnw in zip(w_new, dw_new)]
        self.weights = [w-(ita/len(miniBatch))*nw for w, nw in zip(self.weights, w_new)]
        self.biases = [b-(ita/len(miniBatch))*nb for b, nb in zip(self.biases, b_new)]

    def backwards(self, x, y):
        b_new = [np.zeros(b.shape) for b in self.biases]
        w_new = [np.zeros(w.shape) for w in self.weights]
        
        activation = x
        activations = [x] 
        zs = []
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w,activation)+b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)
        
        delta = self.loss(activations[-1], y) * sigmoidPrime(zs[-1])
        b_new[-1] = delta
        w_new[-1] = np.dot(delta, activations[-2].transpose())
        
        for l in range(2, self.numLayers):
            z = zs[-l]
            sp = sigmoidPrime(z)
            delta = np.dot(self.weights[-l+1].transpose(), delta) * sp
            b_new[-l] = delta
            w_new[-l] = np.dot(delta, activations[-l-1].transpose())
        return (b_new, w_new)

    def evaluate(self, testData):
        result = [(np.argmax(self.forward(x)), y) for (x, y) in testData]
        return sum(int(x == y) for (x, y) in result)

    def loss(self, activations, y):
        return (activations-y)


def sigmoid(z):
    return 1.0/(1.0+np.exp(-z))

def sigmoidPrime(z):
    return sigmoid(z)*(1-sigmoid(z))
