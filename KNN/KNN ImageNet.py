from bs4 import BeautifulSoup
import requests
import numpy as np
import cv2
import urllib
import os
import math
import matplotlib.pyplot as plt
import operator

def urlToImage(url):
	resp = urllib.request.urlopen(url)
	image = np.asarray(bytearray(resp.read()), dtype="uint8")
	image = cv2.imdecode(image, cv2.IMREAD_COLOR)
	return image

def retrieveDataSynset(synsetID, systemPath, synsetName, trainingNum, validationNum):
	synsetUrls = requests.get('http://www.image-net.org/api/text/imagenet.synset.geturls?wnid='+synsetID)
	soup = BeautifulSoup(synsetUrls.content, 'html.parser')
	urls = str(soup).split('\r\n')

	saveImages(trainingNum, urls, systemPath+'/training/'+synsetName)
	saveImages(validationNum, urls, systemPath+'/validation/'+synsetName, trainingNum)

def saveImages(rangeNum, urls, path, offset = 0):
	for progress in range(rangeNum):
		if (progress % 10 == 0):
			print(str(progress)+' images saved')
		if not urls[progress+offset] == None:
			try:
				image = urlToImage(urls[progress+offset])
				if (len(image.shape)) == 3:
					savePath = path+'/img'+str(progress)+'.jpg'
					cv2.imwrite(savePath, image)
			except:
				None

def loadSynsetImages(systemPath, num, synsetName, setType):
	synset = []
	for x in range(num):
		try:
			image = checkImage(systemPath, x, synsetName, setType)
			if not image is None:
				image = cv2.resize(image, (500,350))
				synset.append(image)
		except:
			None
	return synset

def checkImage(systemPath, x, synsetName, setType):
	flickrNotFoundImage = cv2.imread(systemPath+'/flickrNotFound.jpg', cv2.IMREAD_COLOR)
	image = cv2.imread(systemPath+setType+synsetName+'/img'+str(x)+'.jpg', cv2.IMREAD_COLOR)
	if np.array_equal(image, flickrNotFoundImage):
		os.remove(systemPath+setType+synsetName+'/img'+str(x)+'.jpg')
	else:
		return image

def retrieveData(systemPath, trainingNum, validationNum):
	retrieveDataSynset('n04194289', systemPath, 'ships', trainingNum, validationNum)
	retrieveDataSynset('n02834778', systemPath, 'bikes', trainingNum, validationNum)

def euclideanDistance(image1, image2, dimension):
	distance = 0
	for x in range(dimension):
		diff = np.power(image1[:,:,x] - image2[:,:,x], 2)
		distance += math.sqrt(np.sum(diff))
	return distance

def getNeighbours(synsetImages, image, k):
	distances = []
	dimension = image.shape[2]
	for x in range(len(synsetImages)):
		dist = euclideanDistance(image, synsetImages[x], dimension)
		distances.append((synsetImages[x], dist))
	distances.sort(key=operator.itemgetter(1))
	neighbours = []
	for x in range(k):
		neighbours.append(distances[x])
	return neighbours

def test():
	ships = loadSynsetImages(systemPath, trainingNum, 'ships', '/training/')
	bikes = loadSynsetImages(systemPath, trainingNum, 'bikes', '/training/')
	testImage = cv2.resize(cv2.imread(systemPath+'/validation/ships/img5.jpg', cv2.IMREAD_COLOR), (500,350))

	k = 1
	shipsNeighbours = getNeighbours(ships, testImage, k)
	bikesNeighbours = getNeighbours(bikes, testImage, k)

	print('Dist ships:')
	print(shipsNeighbours[0][1])
	cv2.namedWindow('image', cv2.WINDOW_NORMAL)
	cv2.imshow('image', shipsNeighbours[0][0])
	cv2.waitKey(0)
	cv2.destroyAllWindows()
	print('Dist Bikes:')
	print(bikesNeighbours[0][1])
	cv2.imshow('image', bikesNeighbours[0][0])
	cv2.waitKey(0)
	cv2.destroyAllWindows()

def evalPerformance():
	ships = loadSynsetImages(systemPath, trainingNum, 'ships', '/training/')
	bikes = loadSynsetImages(systemPath, trainingNum, 'bikes', '/training/')
	shipsValidation = loadSynsetImages(systemPath, validationNum, 'ships', '/validation/')
	bikesValidation = loadSynsetImages(systemPath, validationNum, 'bikes', '/validation/')

	k = 1

	correctShips = 0
	for i, image in enumerate(shipsValidation):
		if (i%10==0):
			print(str(i)+' ships validation images processed')
		shipsNeighbours = getNeighbours(ships, image, k)
		bikesNeighbours = getNeighbours(bikes, image, k)
		if np.mean(list(map(operator.itemgetter(1),shipsNeighbours))) < np.mean(list(map(operator.itemgetter(1),bikesNeighbours))):
			correctShips += 1

	print('Ships')
	print(str((correctShips/len(shipsValidation))*100)+' %')

	correctBikes = 0
	for i, image in enumerate(bikesValidation):
		if (i%10==0):
			print(str(i)+' bikes validation images processed')
		shipsNeighbours = getNeighbours(ships, image, k)
		bikesNeighbours = getNeighbours(bikes, image, k)
		if np.mean(list(map(operator.itemgetter(1),shipsNeighbours))) > np.mean(list(map(operator.itemgetter(1),bikesNeighbours))):
			correctBikes += 1

	print('Bikes')
	print(str((correctBikes/len(bikesValidation))*100)+' %')





trainingNum, validationNum = 1100, 100
systemPath = 'C:/Users/josel/Documents/Informatica/Deep Learning/image-recognition/KNN'

## Descarga la coleccion de imagenes de ImageNet
retrieveData(systemPath, trainingNum, validationNum) 
evalPerformance()
## Muestra imagenes para un ejemplo en concreto
# test() 