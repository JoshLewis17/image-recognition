import pickle
import numpy as np
import cv2

def unpickle(file):
		with open(file, 'rb') as fo:
				dict = pickle.load(fo, encoding='bytes')
		return dict

def retrieveBatches(systemPath):
	training_data = merge_dicts(unpickle(systemPath+'data_batch_1'),unpickle(systemPath+'data_batch_2'))
	training_data = merge_dicts(training_data,unpickle(systemPath+'data_batch_3'))
	training_data = merge_dicts(training_data,unpickle(systemPath+'data_batch_4'))
	training_data = merge_dicts(training_data,unpickle(systemPath+'data_batch_1'))
	# training_data = unpickle(systemPath+'data_batch_1')
	test_data = unpickle(systemPath+'test_batch')
	return training_data, test_data

def merge_dicts(dict1, dict2):
	dict2.pop(b'batch_label')
	keys = dict2.keys()
	return dict((k, np.concatenate((dict1.get(k), dict2.get(k)), axis=0)) for k in keys)

def calculateLoss(scores, y):
	delta = 1.0
	margins = np.maximum(0, scores - scores[y] + delta)
	margins[y] = 0
	# return np.sum(margins)
	return margins

def L(X, y, W):
	lamba = 0.001
	collectionScores = W.dot(X) # shape -> (10,50000)
	margins = np.array(list(map(calculateLoss, collectionScores.T, y)))
	losses = np.sum(margins, axis=1)
	return np.mean(losses) + lamba * np.sum(np.square(W)), margins

def calculateGradient(X,y,W):
	loss, margins = L(X,y,W)
	grad = np.zeros(W.shape)
	mean = 1/50000
	margins[margins > 0] = mean
	grad = margins.T.dot(X.T)
	return grad

# def eval_numerical_gradient(L, X,y,W):
# 	fx = L(X,y,W)
# 	grad = np.zeros(W.shape)
# 	h = 0.00001

# 	it = np.nditer(W, flags=['multi_index'], op_flags=['readwrite'])
# 	while not it.finished:
# 		ix = it.multi_index
# 		old_value = W[ix]
# 		W[ix] = old_value + h
# 		fxh = L(X,y,W)
# 		W[ix] = old_value
# 		grad[ix] = (fxh - fx) / h
# 		print(grad[ix])
# 		it.iternext()
# 	return grad

def train(X,y,W,iterations,ita,test_data=None):
	for i in range(iterations):
		grad = calculateGradient(X,y,W)
		W = W - ita*grad
		if test_data:
			test = test_data[b'data'].T
			test = np.append(test, np.ones((1,10000)), axis=0)
			evaluate(test, test_data[b'labels'], W)
		print('Iteration '+str(i)+' New loss: '+str(L(X,y,W)[0]))
	return W

def evaluate(X,y,W):
	scores = W.dot(X)
	predictedClasses = np.argmax(scores, axis=0)
	y = np.array(y)
	per = np.sum(np.equal(y, predictedClasses))/100
	print('Test: '+str(per)+' %')


systemPath = 'C:/Users/josel/Documents/Informatica/Deep Learning/SVM/cifar-10-python/cifar-10-batches-py/'
training_data, test_data = retrieveBatches(systemPath)

X = training_data[b'data'].T
X = np.append(X, np.ones((1,50000)), axis=0)
y = training_data[b'labels']
W = np.random.rand(10,3073) * 0.001

print('Loss: '+str(L(X,y,W)[0]))
iterations = 100
ita = 5e-8
W = train(X,y,W,iterations,ita,test_data)

# evaluate(test, test_data[b'labels'], W)

# df = eval_numerical_gradient(L, X,y,W)

# loss_original = L(X,y,W)[0]
# # print('original loss: %f' % (loss_original, ))
# print('Original loss')
# print(loss_original)

# # lets see the effect of multiple step sizes
# for step_size_log in [-10, -9, -8, -7, -6, -5,-4,-3,-2,-1]:
#   step_size = 10 ** step_size_log
#   W_new = W - step_size * df # new position in the weight space
#   loss_new = L(X,y,W_new)[0]
#   print('for step size %f new loss: %f' % (step_size, loss_new))

  ## Numerical evaluation of gradient prints
  #
  # Original loss
  # 20.205688361956486
  # for step size 0.000000 new loss: 20.194158
  # for step size 0.000000 new loss: 20.090488
  # for step size 0.000000 new loss: 19.062580
  # for step size 0.000000 new loss: 11.491494
  # for step size 0.000001 new loss: 116.562897
  # for step size 0.000010 new loss: 1251.259089
  # for step size 0.000100 new loss: 12599.710088
  # for step size 0.001000 new loss: 126084.293430
  # for step size 0.010000 new loss: 1260930.135056
  # for step size 0.100000 new loss: 12609388.551874

# X = np.random.rand(3072,5)
# W = np.random.rand(10,3073)
# X = np.append(X, np.ones((1,5)), axis=0)
# y = np.random.randint(10, size=5) #SHAPE (50000,)